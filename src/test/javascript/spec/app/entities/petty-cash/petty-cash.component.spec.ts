/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DemoTestModule } from '../../../test.module';
import { PettyCashComponent } from 'app/entities/petty-cash/petty-cash.component';
import { PettyCashService } from 'app/entities/petty-cash/petty-cash.service';
import { PettyCash } from 'app/shared/model/petty-cash.model';

describe('Component Tests', () => {
    describe('PettyCash Management Component', () => {
        let comp: PettyCashComponent;
        let fixture: ComponentFixture<PettyCashComponent>;
        let service: PettyCashService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DemoTestModule],
                declarations: [PettyCashComponent],
                providers: []
            })
                .overrideTemplate(PettyCashComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PettyCashComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PettyCashService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PettyCash(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.pettyCashes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
