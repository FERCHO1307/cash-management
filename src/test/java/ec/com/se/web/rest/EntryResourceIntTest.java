package ec.com.se.web.rest;

import ec.com.se.DemoApp;

import ec.com.se.domain.Entry;
import ec.com.se.repository.EntryRepository;
import ec.com.se.service.EntryService;
import ec.com.se.web.rest.errors.ExceptionTranslator;
import ec.com.se.service.dto.EntryCriteria;
import ec.com.se.service.EntryQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static ec.com.se.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EntryResource REST controller.
 *
 * @see EntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApp.class)
public class EntryResourceIntTest {

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(0.01);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(1);

    private static final Instant DEFAULT_DATE_ENTRY = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_ENTRY = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final BigDecimal DEFAULT_PREVIOUS_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PREVIOUS_BALANCE = new BigDecimal(2);

    @Autowired
    private EntryRepository entryRepository;

    

    @Autowired
    private EntryService entryService;

    @Autowired
    private EntryQueryService entryQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEntryMockMvc;

    private Entry entry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EntryResource entryResource = new EntryResource(entryService, entryQueryService);
        this.restEntryMockMvc = MockMvcBuilders.standaloneSetup(entryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entry createEntity(EntityManager em) {
        Entry entry = new Entry()
            .amount(DEFAULT_AMOUNT)
            .dateEntry(DEFAULT_DATE_ENTRY)
            .previousBalance(DEFAULT_PREVIOUS_BALANCE);
        return entry;
    }

    @Before
    public void initTest() {
        entry = createEntity(em);
    }

    @Test
    @Transactional
    public void createEntry() throws Exception {
        int databaseSizeBeforeCreate = entryRepository.findAll().size();

        // Create the Entry
        restEntryMockMvc.perform(post("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isCreated());

        // Validate the Entry in the database
        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeCreate + 1);
        Entry testEntry = entryList.get(entryList.size() - 1);
        assertThat(testEntry.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testEntry.getDateEntry()).isEqualTo(DEFAULT_DATE_ENTRY);
        assertThat(testEntry.getPreviousBalance()).isEqualTo(DEFAULT_PREVIOUS_BALANCE);
    }

    @Test
    @Transactional
    public void createEntryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = entryRepository.findAll().size();

        // Create the Entry with an existing ID
        entry.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEntryMockMvc.perform(post("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isBadRequest());

        // Validate the Entry in the database
        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = entryRepository.findAll().size();
        // set the field null
        entry.setAmount(null);

        // Create the Entry, which fails.

        restEntryMockMvc.perform(post("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isBadRequest());

        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateEntryIsRequired() throws Exception {
        int databaseSizeBeforeTest = entryRepository.findAll().size();
        // set the field null
        entry.setDateEntry(null);

        // Create the Entry, which fails.

        restEntryMockMvc.perform(post("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isBadRequest());

        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPreviousBalanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = entryRepository.findAll().size();
        // set the field null
        entry.setPreviousBalance(null);

        // Create the Entry, which fails.

        restEntryMockMvc.perform(post("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isBadRequest());

        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEntries() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList
        restEntryMockMvc.perform(get("/api/entries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entry.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].dateEntry").value(hasItem(DEFAULT_DATE_ENTRY.toString())))
            .andExpect(jsonPath("$.[*].previousBalance").value(hasItem(DEFAULT_PREVIOUS_BALANCE.intValue())));
    }
    

    @Test
    @Transactional
    public void getEntry() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get the entry
        restEntryMockMvc.perform(get("/api/entries/{id}", entry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(entry.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()))
            .andExpect(jsonPath("$.dateEntry").value(DEFAULT_DATE_ENTRY.toString()))
            .andExpect(jsonPath("$.previousBalance").value(DEFAULT_PREVIOUS_BALANCE.intValue()));
    }

    @Test
    @Transactional
    public void getAllEntriesByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where amount equals to DEFAULT_AMOUNT
        defaultEntryShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the entryList where amount equals to UPDATED_AMOUNT
        defaultEntryShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllEntriesByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultEntryShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the entryList where amount equals to UPDATED_AMOUNT
        defaultEntryShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllEntriesByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where amount is not null
        defaultEntryShouldBeFound("amount.specified=true");

        // Get all the entryList where amount is null
        defaultEntryShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllEntriesByDateEntryIsEqualToSomething() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where dateEntry equals to DEFAULT_DATE_ENTRY
        defaultEntryShouldBeFound("dateEntry.equals=" + DEFAULT_DATE_ENTRY);

        // Get all the entryList where dateEntry equals to UPDATED_DATE_ENTRY
        defaultEntryShouldNotBeFound("dateEntry.equals=" + UPDATED_DATE_ENTRY);
    }

    @Test
    @Transactional
    public void getAllEntriesByDateEntryIsInShouldWork() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where dateEntry in DEFAULT_DATE_ENTRY or UPDATED_DATE_ENTRY
        defaultEntryShouldBeFound("dateEntry.in=" + DEFAULT_DATE_ENTRY + "," + UPDATED_DATE_ENTRY);

        // Get all the entryList where dateEntry equals to UPDATED_DATE_ENTRY
        defaultEntryShouldNotBeFound("dateEntry.in=" + UPDATED_DATE_ENTRY);
    }

    @Test
    @Transactional
    public void getAllEntriesByDateEntryIsNullOrNotNull() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where dateEntry is not null
        defaultEntryShouldBeFound("dateEntry.specified=true");

        // Get all the entryList where dateEntry is null
        defaultEntryShouldNotBeFound("dateEntry.specified=false");
    }

    @Test
    @Transactional
    public void getAllEntriesByPreviousBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where previousBalance equals to DEFAULT_PREVIOUS_BALANCE
        defaultEntryShouldBeFound("previousBalance.equals=" + DEFAULT_PREVIOUS_BALANCE);

        // Get all the entryList where previousBalance equals to UPDATED_PREVIOUS_BALANCE
        defaultEntryShouldNotBeFound("previousBalance.equals=" + UPDATED_PREVIOUS_BALANCE);
    }

    @Test
    @Transactional
    public void getAllEntriesByPreviousBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where previousBalance in DEFAULT_PREVIOUS_BALANCE or UPDATED_PREVIOUS_BALANCE
        defaultEntryShouldBeFound("previousBalance.in=" + DEFAULT_PREVIOUS_BALANCE + "," + UPDATED_PREVIOUS_BALANCE);

        // Get all the entryList where previousBalance equals to UPDATED_PREVIOUS_BALANCE
        defaultEntryShouldNotBeFound("previousBalance.in=" + UPDATED_PREVIOUS_BALANCE);
    }

    @Test
    @Transactional
    public void getAllEntriesByPreviousBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        entryRepository.saveAndFlush(entry);

        // Get all the entryList where previousBalance is not null
        defaultEntryShouldBeFound("previousBalance.specified=true");

        // Get all the entryList where previousBalance is null
        defaultEntryShouldNotBeFound("previousBalance.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEntryShouldBeFound(String filter) throws Exception {
        restEntryMockMvc.perform(get("/api/entries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entry.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].dateEntry").value(hasItem(DEFAULT_DATE_ENTRY.toString())))
            .andExpect(jsonPath("$.[*].previousBalance").value(hasItem(DEFAULT_PREVIOUS_BALANCE.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEntryShouldNotBeFound(String filter) throws Exception {
        restEntryMockMvc.perform(get("/api/entries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingEntry() throws Exception {
        // Get the entry
        restEntryMockMvc.perform(get("/api/entries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEntry() throws Exception {
        // Initialize the database
        entryService.save(entry);

        int databaseSizeBeforeUpdate = entryRepository.findAll().size();

        // Update the entry
        Entry updatedEntry = entryRepository.findById(entry.getId()).get();
        // Disconnect from session so that the updates on updatedEntry are not directly saved in db
        em.detach(updatedEntry);
        updatedEntry
            .amount(UPDATED_AMOUNT)
            .dateEntry(UPDATED_DATE_ENTRY)
            .previousBalance(UPDATED_PREVIOUS_BALANCE);

        restEntryMockMvc.perform(put("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEntry)))
            .andExpect(status().isOk());

        // Validate the Entry in the database
        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeUpdate);
        Entry testEntry = entryList.get(entryList.size() - 1);
        assertThat(testEntry.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testEntry.getDateEntry()).isEqualTo(UPDATED_DATE_ENTRY);
        assertThat(testEntry.getPreviousBalance()).isEqualTo(UPDATED_PREVIOUS_BALANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingEntry() throws Exception {
        int databaseSizeBeforeUpdate = entryRepository.findAll().size();

        // Create the Entry

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEntryMockMvc.perform(put("/api/entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entry)))
            .andExpect(status().isBadRequest());

        // Validate the Entry in the database
        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEntry() throws Exception {
        // Initialize the database
        entryService.save(entry);

        int databaseSizeBeforeDelete = entryRepository.findAll().size();

        // Get the entry
        restEntryMockMvc.perform(delete("/api/entries/{id}", entry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Entry> entryList = entryRepository.findAll();
        assertThat(entryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Entry.class);
        Entry entry1 = new Entry();
        entry1.setId(1L);
        Entry entry2 = new Entry();
        entry2.setId(entry1.getId());
        assertThat(entry1).isEqualTo(entry2);
        entry2.setId(2L);
        assertThat(entry1).isNotEqualTo(entry2);
        entry1.setId(null);
        assertThat(entry1).isNotEqualTo(entry2);
    }
}
