import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DemoRequestModule } from './request/request.module';
import { DemoEntryModule } from './entry/entry.module';
import { DemoPettyCashModule } from './petty-cash/petty-cash.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        DemoRequestModule,
        DemoEntryModule,
        DemoPettyCashModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoEntityModule {}
