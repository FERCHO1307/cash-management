import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPettyCash } from 'app/shared/model/petty-cash.model';
import { Principal } from 'app/core';
import { PettyCashService } from './petty-cash.service';

@Component({
    selector: 'jhi-petty-cash',
    templateUrl: './petty-cash.component.html'
})
export class PettyCashComponent implements OnInit, OnDestroy {
    pettyCashes: IPettyCash[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private pettyCashService: PettyCashService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.pettyCashService.query().subscribe(
            (res: HttpResponse<IPettyCash[]>) => {
                this.pettyCashes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPettyCashes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPettyCash) {
        return item.id;
    }

    registerChangeInPettyCashes() {
        this.eventSubscriber = this.eventManager.subscribe('pettyCashListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
