import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DemoSharedModule } from 'app/shared';
import {
    RequestComponent,
    RequestDetailComponent,
    RequestUpdateComponent,
    RequestDeletePopupComponent,
    RequestDeleteDialogComponent,
    requestRoute,
    requestPopupRoute
} from './';
import { RequestStatusPipe } from 'app/shared/pipes/status.pipe';

const ENTITY_STATES = [...requestRoute, ...requestPopupRoute];

@NgModule({
    imports: [DemoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RequestComponent,
        RequestDetailComponent,
        RequestUpdateComponent,
        RequestDeleteDialogComponent,
        RequestDeletePopupComponent,
        RequestStatusPipe
    ],
    entryComponents: [RequestComponent, RequestUpdateComponent, RequestDeleteDialogComponent, RequestDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DemoRequestModule {}
