import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IRequest, RequestStatus } from 'app/shared/model/request.model';
import { RequestService } from './request.service';

@Component({
    selector: 'jhi-request-update',
    templateUrl: './request-update.component.html'
})
export class RequestUpdateComponent implements OnInit {
    private _request: IRequest;
    isSaving: boolean;
    requestDate: string;
    reviewDate: string;

    constructor(private requestService: RequestService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ request }) => {
            this.request = request;
        });
    }

    previousState() {
        window.history.back();
    }

    approve() {
        this.isSaving = true;
        this.request.status = RequestStatus.APPROVED;
        if (this.request.id !== undefined) {
            this.subscribeToSaveResponse(this.requestService.update(this.request));
        }
    }

    reject() {
        this.isSaving = true;
        this.request.status = RequestStatus.REJECTED;
        if (this.request.id !== undefined) {
            this.subscribeToSaveResponse(this.requestService.update(this.request));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRequest>>) {
        result.subscribe((res: HttpResponse<IRequest>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get request() {
        return this._request;
    }

    set request(request: IRequest) {
        this._request = request;
        this.requestDate = moment(request.requestDate).format(DATE_TIME_FORMAT);
        this.reviewDate = moment(request.reviewDate).format(DATE_TIME_FORMAT);
    }
}
