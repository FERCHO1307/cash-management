import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRequest } from 'app/shared/model/request.model';

type EntityResponseType = HttpResponse<IRequest>;
type EntityArrayResponseType = HttpResponse<IRequest[]>;

@Injectable({ providedIn: 'root' })
export class RequestService {
    private resourceUrl = SERVER_API_URL + 'api/requests';

    constructor(private http: HttpClient) {}

    create(request: IRequest): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(request);
        return this.http
            .post<IRequest>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(request: IRequest): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(request);
        return this.http
            .put<IRequest>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IRequest>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IRequest[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(request: IRequest): IRequest {
        const copy: IRequest = Object.assign({}, request, {
            requestDate: request.requestDate != null && request.requestDate.isValid() ? request.requestDate.toJSON() : null,
            reviewDate: request.reviewDate != null && request.reviewDate.isValid() ? request.reviewDate.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.requestDate = res.body.requestDate != null ? moment(res.body.requestDate) : null;
        res.body.reviewDate = res.body.reviewDate != null ? moment(res.body.reviewDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((request: IRequest) => {
            request.requestDate = request.requestDate != null ? moment(request.requestDate) : null;
            request.reviewDate = request.reviewDate != null ? moment(request.reviewDate) : null;
        });
        return res;
    }
}
