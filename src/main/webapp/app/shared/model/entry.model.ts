import { Moment } from 'moment';

export interface IEntry {
    id?: number;
    amount?: number;
    dateEntry?: Moment;
    previousBalance?: number;
}

export class Entry implements IEntry {
    constructor(public id?: number, public amount?: number, public dateEntry?: Moment, public previousBalance?: number) {}
}
