import { Moment } from 'moment';

export const enum RequestStatus {
    REQUESTED = 'REQUESTED',
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED'
}

export interface IRequest {
    id?: number;
    name?: string;
    amount?: number;
    requestDate?: Moment;
    status?: RequestStatus;
    reviewDate?: Moment;
    description?: string;
}

export class Request implements IRequest {
    constructor(
        public id?: number,
        public name?: string,
        public amount?: number,
        public requestDate?: Moment,
        public status?: RequestStatus,
        public reviewDate?: Moment,
        public description?: string
    ) {}
}
