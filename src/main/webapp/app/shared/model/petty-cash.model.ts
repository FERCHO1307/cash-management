import { Moment } from 'moment';

export interface IPettyCash {
    id?: number;
    amount?: number;
    dateUpdate?: Moment;
    code?: string;
}

export class PettyCash implements IPettyCash {
    constructor(public id?: number, public amount?: number, public dateUpdate?: Moment, public code?: string) {}
}
