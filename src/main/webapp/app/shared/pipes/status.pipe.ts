import { Pipe, PipeTransform } from '@angular/core';
import { RequestStatus } from 'app/shared/model/request.model';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({ name: 'requestStatus' })
export class RequestStatusPipe implements PipeTransform {
    transform(value: string): string {
        if (value === RequestStatus.REQUESTED) {
            return 'SOLICITADO';
        } else if (value === RequestStatus.APPROVED) {
            return 'APROBADO';
        } else if (value === RequestStatus.REJECTED) {
            return 'RECHAZADO';
        }
    }
}
