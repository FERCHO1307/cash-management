package ec.com.se.domain.enumeration;

/**
 * The RequestStatus enumeration.
 */
public enum RequestStatus {
    REQUESTED, APPROVED, REJECTED
}
