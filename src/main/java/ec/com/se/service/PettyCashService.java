package ec.com.se.service;

import ec.com.se.domain.PettyCash;
import ec.com.se.repository.PettyCashRepository;
import ec.com.se.web.rest.errors.BadRequestAlertException;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing PettyCash.
 */
@Service
@Transactional
public class PettyCashService {

    private final Logger log = LoggerFactory.getLogger(PettyCashService.class);
    private static final String CODE_PETTY_CASH = "CPT";
    private static final String ENTITY_NAME = "pettyCash";

    private final PettyCashRepository pettyCashRepository;

    public PettyCashService(PettyCashRepository pettyCashRepository) {
        this.pettyCashRepository = pettyCashRepository;
    }

    /**
     * Save a pettyCash.
     *
     * @param pettyCash the entity to save
     * @return the persisted entity
     */
    public PettyCash save(PettyCash pettyCash) {
        log.debug("Request to save PettyCash : {}", pettyCash);        return pettyCashRepository.save(pettyCash);
    }

    /**
     * Get all the pettyCashes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PettyCash> findAll() {
        log.debug("Request to get all PettyCashes");
        return pettyCashRepository.findAll();
    }


    /**
     * Get one pettyCash by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<PettyCash> findOne(Long id) {
        log.debug("Request to get PettyCash : {}", id);
        return pettyCashRepository.findById(id);
    }

    /**
     * Delete the pettyCash by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PettyCash : {}", id);
        pettyCashRepository.deleteById(id);
    }

    /**
     * find PettyCashBalance by code
     *
     * @return PettyCash
     */
    public PettyCash findPettyCashBalance(){
        Optional<PettyCash> pettyCashOptional = pettyCashRepository.findByCode(CODE_PETTY_CASH);
        if(pettyCashOptional.isPresent()){
            return pettyCashOptional.get();
        }else{
            throw new BadRequestAlertException("No se encuentra código de caja chica.", ENTITY_NAME, "idNotFound");
        }
    }
}
