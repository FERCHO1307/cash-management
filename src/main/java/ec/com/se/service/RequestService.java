package ec.com.se.service;

import ec.com.se.domain.PettyCash;
import ec.com.se.domain.Request;
import ec.com.se.domain.enumeration.RequestStatus;
import ec.com.se.repository.PettyCashRepository;
import ec.com.se.repository.RequestRepository;
import ec.com.se.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.Instant;
import java.util.Optional;
/**
 * Service Implementation for managing Request.
 */
@Service
@Transactional
public class RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestService.class);
    private static final String ENTITY_NAME = "request";

    private final RequestRepository requestRepository;
    private final PettyCashService pettyCashService;

    public RequestService(RequestRepository requestRepository, PettyCashService pettyCashService) {
        this.requestRepository = requestRepository;
        this.pettyCashService = pettyCashService;
    }

    /**
     * Save a request.
     *
     * @param request the entity to save
     * @return the persisted entity
     */
    public Request save(Request request) {
        log.debug("Request to save Request : {}", request);
        request.setRequestDate(Instant.now());
        request.setStatus(RequestStatus.REQUESTED);
        return requestRepository.save(request);
    }

    /**
     * Update a request
     *
     * @param request
     * @return
     */
    public Request update(Request request) {
        Request result = null;
        log.debug("Request to update Request : {}", request);
        request.setReviewDate(Instant.now());
        if(request.getStatus().equals(RequestStatus.APPROVED)){
            PettyCash pettyCash = pettyCashService.findPettyCashBalance();
            if(request.getAmount().compareTo(pettyCash.getAmount()) > 0){
                throw new BadRequestAlertException("El monto solicitado excede el monto disponible.", ENTITY_NAME, "amountExceeded");
            }else{
                pettyCash.setAmount(pettyCash.getAmount().subtract(request.getAmount()));
                pettyCashService.save(pettyCash);
            }
        }
        result = requestRepository.save(request);
        return result;
    }

    /**
     * Get all the requests.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Request> findAll(Pageable pageable) {
        log.debug("Request to get all Requests");
        return requestRepository.findAll(pageable);
    }


    /**
     * Get one request by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Request> findOne(Long id) {
        log.debug("Request to get Request : {}", id);
        return requestRepository.findById(id);
    }

    /**
     * Delete the request by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Request : {}", id);
        requestRepository.deleteById(id);
    }
}
