package ec.com.se.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;




/**
 * Criteria class for the Entry entity. This class is used in EntryResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /entries?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EntryCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private BigDecimalFilter amount;

    private InstantFilter dateEntry;

    private BigDecimalFilter previousBalance;

    public EntryCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BigDecimalFilter getAmount() {
        return amount;
    }

    public void setAmount(BigDecimalFilter amount) {
        this.amount = amount;
    }

    public InstantFilter getDateEntry() {
        return dateEntry;
    }

    public void setDateEntry(InstantFilter dateEntry) {
        this.dateEntry = dateEntry;
    }

    public BigDecimalFilter getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(BigDecimalFilter previousBalance) {
        this.previousBalance = previousBalance;
    }

    @Override
    public String toString() {
        return "EntryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (dateEntry != null ? "dateEntry=" + dateEntry + ", " : "") +
                (previousBalance != null ? "previousBalance=" + previousBalance + ", " : "") +
            "}";
    }

}
