package ec.com.se.service;

import ec.com.se.domain.Entry;
import ec.com.se.domain.PettyCash;
import ec.com.se.repository.EntryRepository;
import ec.com.se.repository.PettyCashRepository;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
/**
 * Service Implementation for managing Entry.
 */
@Service
@Transactional
public class EntryService {

    private static final String CODE_PETTY_CASH = "CPT";
    private final Logger log = LoggerFactory.getLogger(EntryService.class);

    private final EntryRepository entryRepository;
    private final PettyCashService pettyCashService;

    public EntryService(EntryRepository entryRepository, PettyCashService pettyCashService) {
        this.entryRepository = entryRepository;
        this.pettyCashService = pettyCashService;
    }

    /**
     * Save a entry.
     *
     * @param entry the entity to save
     * @return the persisted entity
     */
    public Entry save(Entry entry) {
        log.debug("Request to save Entry : {}", entry);
        Instant instant = Instant.now();
        PettyCash pettyCash = pettyCashService.findPettyCashBalance();

        //Update Entry Information
        BigDecimal cashPreview = pettyCash.getAmount();
        entry.setPreviousBalance(cashPreview);
        entry.setDateEntry(instant);

        //Update PettyCash Information
        pettyCash.setAmount(pettyCash.getAmount().add(entry.getAmount()));
        pettyCash.setDateUpdate(instant);
        pettyCashService.save(pettyCash);

        return entryRepository.save(entry);
    }

    /**
     * Get all the entries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Entry> findAll(Pageable pageable) {
        log.debug("Request to get all Entries");
        return entryRepository.findAll(pageable);
    }


    /**
     * Get one entry by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Entry> findOne(Long id) {
        log.debug("Request to get Entry : {}", id);
        return entryRepository.findById(id);
    }

    /**
     * Delete the entry by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Entry : {}", id);
        entryRepository.deleteById(id);
    }
}
