package ec.com.se.repository;

import ec.com.se.domain.PettyCash;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the PettyCash entity.
 */
@Repository
public interface PettyCashRepository extends JpaRepository<PettyCash, Long> {
    /**
     * Return Optional PettyCash by code
     *
     * @param code
     * @return
     */
    Optional<PettyCash> findByCode(String code);
}
